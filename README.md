# tfeiszt elasticable

### PHP library description
Elasticsearch model abstraction package.

### Dependencies
* PHP ^7.3
* Elasticsearch ^7.0
* ruflin/elastica ^7.0,
* psr/log ^1.1

### Features
* Search by term
* Fuzzy search
* Fluent query builder
* Indexing
* Trait and interface can support your custom ORM models

### Install by composer
```php
{
  "repositories": [
    {
      "type": "vcs",
      "url":  "https://tfeiszt@bitbucket.org/tfeiszt/elasticable.git"
    }
  ],
  "require": {
    "tfeiszt/elasticable": "^1.0"
  }
}
```

### Init elasticable
```php
// composer autoload.php
require_once './vendor/autoload.php';

```

### Basic usage

#### Create a custom elasticable model
```phpregexp
<?php
namespace App\model;

use tfeiszt\elasticable\AbstractElasticableModel;

class MyModel extends AbstractElasticableModel
{
    public function getElasticId()
    {
        return $this->getAttribute('id');
    }

    public static function getElasticMapping() : array
    {
        // Se documentation of mapping: https://www.elastic.co/guide/en/elasticsearch/reference/7.x/mapping.html
        return [
            'id' => [
                'type' => 'integer'
            ],
            'name' => [
                'type' => 'text',
                'analyzer' => 'standard',
            ],
            'category' => [
                'type' => 'keyword',
                'index' => true,
            ],
            'colour' => [
                'type' => 'keyword',
                'index' => true,
            ],
            'price' =>[
                'type' => 'float',
            ],
            'status' => [
                'type' => 'keyword',
                'index' => true,
            ]
        ];
    }
}
```
#### Or make "elasticable" your existing datamodel
You might need to use different result builder class in this case due to dependency injection at your constructor. Please take a look at ElasticableTrait::getElasticIndexClass() method.
```phpregexp
<?php
namespace App\model;

use SomeDbModel; // It can be a a custom DB model, or a Doctrine whatever you wish
use tfeiszt\elasticable\AttributesInterface;
use tfeiszt\elasticable\ElasticableInterface;
use tfeiszt\elasticable\AttributesTrait;
use tfeiszt\elasticable\ElasticableTrait;

class YourDbModel extends SomeDbModel implements
    AttributesInterface, // Add these two interfaces
    ElasticableInterface
{
    use AttributesTrait; // Add this trait, or implement AttributesInterface on your own
    use ElasticableTrait; // Add this trait to implement elasticable methods
    
    public static function getElasticMapping(): array
    {
        return [
            'yourField1' = > [
                'type' => 'keyword',
                'index' => true,
            ], ...
            // implement your elastic mapping in the array structure above
            // See documentation about mapping: https://www.elastic.co/guide/en/elasticsearch/reference/7.x/mapping.html
        ];
    }
    
    // Optional. If your model need dependencies in __construct() you need a custom Builder.
    // Overriding the Index, and the Builder is the easiest way to do it.
    public static function getElasticIndexClass() : string
    {
        return YourIndexClass::class;
    }
    
    public function getElasticId()
    {
        return $this->id; // Return your datamodel id value here
    }
}
```
#### Create an elastic client
```phpregexp
$client = new Client([
    'host' => 'localhost', // your elasticsearch host - no port needed
    'name' => 'myname' // your elasticsearch index prefix (as es7 has no type)
);
```

#### Create an elastic index for your model
```phpregexp
$p = new MyModel();
$p->setElasticaClient($client);
$indexer = new SingleIndexer($p);
$indexer->createIndex();
```
#### Index your model (using fixtures for now)
```phpregexp
$fixtures = [
    ['id' => 1, 'name' => 'Gold diamond ring', 'category' => 'ring', 'colour' => ['yellow', 'blue'], 'price' => 100.00, 'status' => 'active'],
    ['id' => 2, 'name' => 'Silver diamond ring', 'category' => 'ring', 'colour' => ['white', 'blue'], 'price' => 100.00, 'status' => 'active'],
    ['id' => 3, 'name' => 'Rose gold diamond bracelet', 'category' => 'bracelet', 'colour' => ['white', 'blue', 'pink'], 'price' => 100.00, 'status' => 'active']
];
foreach ($fixtures as $fixture) {
    $p = new MyModel();
    $p->setElasticaClient($client); // Elastica client from above
    $p->setAttributes($fixture); // Your data from fixtures or from your real datamodel
    $indexer = new SingleIndexer($p);
    $indexer->updateElasticDocument(true);
}
```
#### Search by simple term
```phpregexp
$q = (new MatchSearch())
    ->setTerm('status') // Field
    ->setValue('active') // Search value
    ->getSearchQuery();

$p = new MyModel();
$p->setElasticaClient($client);
$res = $p->getElasticaClient()->getModelIndex($p)->search($q);
var_dump($res->getResults()); // Array of MyModel
```
#### Search by query term
```phpregexp
$q = (new SimpleSearch())
    ->addField('name') // Field
    ->setQuery('*ring*') // Search query
    ->getSearchQuery();

$p = new MyModel();
$p->setElasticaClient($client);
$res = $p->getElasticaClient()->getModelIndex($p)->search($q);
var_dump($res->getResults()); // Array of MyModel
```
#### Fuzzy search for query term
```phpregexp
$q = (new FuzzySearch())
    ->addField('name') // Field
    ->setQuery('diamont') // Search query - spelling mistake
    ->getSearchQuery();

$p = new MyModel();
$p->setElasticaClient($client);
$res = $p->getElasticaClient()->getModelIndex($p)->search($q);
var_dump($res->getResults()); // Array of MyModel
```
#### Using fluent query
```phpregexp
$q = (new SimpleFluentQuery())
    ->search()->addField('name')->setQuery('*diamond*')->and() // Simple search by pattern
    // ->fuzzy()->addField('name')->setQuery('diamand')->and() // Fuzzy search with seplling mistake
    ->match()->setField('status')->setValue('active') // Exact match 
    ->addAggregation('category')
    ->addSort('price', 'DESC') // do not sort to analysed string type
    ->build();

$p = new MyModel();
$p->setElasticaClient($client);
$res = $p->getElasticaClient()->getModelIndex($p)->search($q);
var_dump($res->getResults()); // Array of MyModel
var_dump($res->getAggregations()) // Array of aggregation buckets
```
#### Pagination
```phpregexp
$p = new MyModel();
$p->setElasticaClient($client);
$res = $p->getElasticaClient()
    ->getModelIndex($p)
    ->setPagination(1, 10) // Pagination here 1'st page, page size is 10
    ->search($q);
var_dump($res->getResults()); // Array of MyModel
var_dump($res->getAggregations()); // Array of aggregation buckets
var_dump($res->getPagination()); // Array of pagination info
```

### Related documentations
Elasticsearch: https://www.elastic.co/guide/en/elasticsearch/reference/7.x/index.html

ruflin/Elastica: https://elastica.io

### License

MIT
