<?php
namespace tfeiszt\elasticable;

use \Elastica\Client as ElasticaClient;
use Psr\Log\LoggerInterface;
use Elastica\Request;
use Elastica\Response;
use Elastica\Exception\ResponseException;

/**
 * Class Client
 * @package tfeiszt\elasticable
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class Client extends ElasticaClient
{
    /**
     * Client constructor.
     * @param array $config
     * @param null $callback
     * @param LoggerInterface|null $logger
     */
    public function __construct(array $config = [], $callback = null, LoggerInterface $logger = null)
    {
        if (!isset($config['host'])) {
            throw new \Exception('Missing required configuration: [host]');
        }
        if (!isset($config['name'])) {
            throw new \Exception('Missing required configuration: [name]');
        }

        parent::__construct($config, $callback, $logger);
    }

    /**
     * @param string $path
     * @param string $method
     * @param array $data
     * @param array $query
     * @param string $contentType
     * @return Response
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function request(
        string $path,
        string $method = Request::GET,
        $data = [],
        array $query = [],
        string $contentType = Request::DEFAULT_CONTENT_TYPE
    ): Response
    {
        try {
            return parent::request($path, $method, $data, $query, $contentType);
        } catch (ResponseException $e) {
            $this->_logger->error('Elastica Request Failure', [
                'request' => $e->getRequest()->toArray(),
                'response' => $e->getResponse()->getFullError(),
            ]);
            throw $e;
        }
    }

    /**
     * @param ElasticableInterface $model
     * @return \tfeiszt\elasticable\Index
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getModelIndex(ElasticableInterface $model)
    {
        $name = $this->getConfig('name');
        $indexClass = $model::getElasticIndexClass();
        $index = new $indexClass($this, $name);
        $index->setModel($model);
        return $index;
    }

    /**
     * @return \Psr\Log\LoggerInterface|\Psr\Log\NullLogger
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getLogger()
    {
        return $this->_logger;
    }
}
