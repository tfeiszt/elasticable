<?php
namespace tfeiszt\elasticable;

use Elastica\Query;
use Elastica\Response;
use Elastica\ResultSet\BuilderInterface;

/**
 * Class Builder
 * @package tfeiszt\elasticable
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class Builder implements BuilderInterface
{
    /**
     * @var ElasticableInterface
     */
    protected $model;

    /**
     * @var Pagination
     */
    protected $pagination = null;

    /**
     * Builder constructor.
     * @param Pagination|null $pagination
     */
    public function __construct(Pagination $pagination = null)
    {
        $this->pagination = $pagination;
    }

    /**
     * @param Response $response
     * @param Query $query
     * @return \Elastica\ResultSet
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function buildResultSet(Response $response, Query $query): \Elastica\ResultSet
    {
        $results = $this->buildResults($response);
        $resultSet = new ResultSet($response, $query, $results);

        if ($this->getModel()) {
            $className = get_class($this->model);
            $resultSet->setModelClass($className);
        }
        if ($this->pagination instanceof Pagination) {
            $resultSet->setPagination($this->pagination);
        }

        return $resultSet;
    }

    /**
     * @param Response $response
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function buildResults(Response $response)
    {
        $data = $response->getData();
        $results = [];

        if (!isset($data['hits']['hits'])) {
            return $results;
        }

        $resultClass = get_class($this->model);
        $index = 0;
        foreach ($data['hits']['hits'] as $hit) {
            $model = new $resultClass();
            if ($model instanceof AttributesInterface && $model instanceof ElasticableInterface) {
                $model->setAttributes($hit['_source']);
                $results[] = $model;
            } else {
                throw new \Exception(get_class($this->model) . " is not supported for " . get_class($this));
            }
        }

        return $results;
    }

    /**
     * @param ElasticableInterface $model
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function setModel(ElasticableInterface $model)
    {
        $this->model = $model;
    }

    /**
     * @return ElasticableInterface
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getModel()
    {
        return $this->model;
    }
}
