<?php
namespace tfeiszt\elasticable;

/**
 * Trait AttributesTrait
 * @package tfeiszt\elasticable
 */
trait AttributesTrait
{
    /**
     * @var array
     */
    protected $attributes = [];

    /**
     * @param array $attributes
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function setAttributes(array $attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * @param string $key
     * @return mixed|null
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getAttribute(string $key)
    {
        return (isset($this->attributes[$key])) ? $this->attributes[$key] : null;
    }

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getAttributes() : array
    {
        return $this->attributes;
    }
}
