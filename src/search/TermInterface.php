<?php
namespace tfeiszt\elasticable\search;

/**
 * Interface TermInterface
 * @package tfeiszt\elasticable\search
 */
interface TermInterface
{
    /**
     * @param string $term
     * @return mixed
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function setField(string $term);

    /**
     * @param $query
     * @return mixed
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function setValue($query);
}
