<?php
namespace tfeiszt\elasticable\search;

use Elastica\Query\BoolQuery;
use Elastica\Query\Term;

/**
 * Class MatchTerm
 * @package tfeiszt\elasticable\search
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class MatchTerm extends AbstractTerm
{
    /**
     * @return BoolQuery
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getSearchQuery() : BoolQuery
    {
        $term = new Term();
        $term->setTerm($this->term, $this->query);
        $bool = new BoolQuery();
        $bool->addMust($term);
        return $bool;
    }
}
