<?php
namespace tfeiszt\elasticable\search;

use Elastica\Query\BoolQuery;

/**
 * Interface QueryableInterface
 * @package tfeiszt\elasticable\search
 */
interface QueryableInterface
{
    /**
     * @return BoolQuery
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getSearchQuery() : BoolQuery;
}
