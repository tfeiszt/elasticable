<?php
namespace tfeiszt\elasticable\search;

use Elastica\Query\BoolQuery;
use tfeiszt\elasticable\query\SimpleFluentQuery;
use tfeiszt\elasticable\query\FluentInterface;

/**
 * Class AbstractSearch
 * @package tfeiszt\elasticable\search
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
abstract class AbstractSearch implements
    QueryableInterface,
    SearchInterface
{
    /**
     * @var string|mixed
     */
    protected $query;
    /**
     * @var array
     */
    protected $fields = [];
    /**
     * @var FluentInterface|null
     */
    protected $parent;

    /**
     * @return BoolQuery
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    abstract public function getSearchQuery() : BoolQuery;

    /**
     * AbstractSearch constructor.
     * @param FluentInterface|null $parent
     */
    public function __construct(FluentInterface $parent = null) {
        $this->parent = $parent;
    }

    /**
     * @param $name
     * @param $arguments
     * @return $this|false|mixed
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    function __call($name, $arguments){
        if ($this->parent instanceof SimpleFluentQuery) {
            $result = call_user_func_array(array($this->parent, $name), $arguments);
            if ($result instanceof SimpleFluentQuery) {
                return $this;
            }
            return $result;
        }
    }

    /**
     * @param string $query
     * @return $this
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function setQuery($query)
    {
        $this->query = $query;
        return $this;
    }

    /**
     * @param array $fields
     * @return $this
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function setFields(array $fields = [])
    {
        $this->fields = $fields;
        return $this;
    }

    /**
     * @param string $field
     * @return $this
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function addField(string $field)
    {
        $this->fields = array_merge($this->fields, [$field]);
        return $this;
    }
}
