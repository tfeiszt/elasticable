<?php
namespace tfeiszt\elasticable\search;

use Elastica\Query\BoolQuery;
use Elastica\Query\QueryString;

/**
 * Class SimpleSearch
 * @package tfeiszt\elasticable\search
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class SimpleSearch extends AbstractSearch
{
    /**
     * @return BoolQuery
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getSearchQuery() : BoolQuery
    {
        $queryString = new QueryString();
        $queryString->setQuery($this->query);
        $queryString->setFields($this->fields);
        $queryString->setDefaultOperator('AND');
        $bool = new BoolQuery();
        $bool->addMust($queryString);
        return $bool;
    }
}
