<?php
namespace tfeiszt\elasticable\search;

use Elastica\Query\BoolQuery;
use Elastica\Query\MultiMatch;

/**
 * Class FuzzySearch
 * @package tfeiszt\elasticable\search
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class FuzzySearch extends AbstractSearch
{
    /**
     * @return BoolQuery
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getSearchQuery() : BoolQuery
    {
        $queryMultiMatch = new MultiMatch();
        $queryMultiMatch->setQuery($this->query);
        $queryMultiMatch->setFields($this->fields);
        $queryMultiMatch->setFuzziness('AUTO');
        $bool = new BoolQuery();
        $bool->addMust($queryMultiMatch);
        return $bool;
    }
}
