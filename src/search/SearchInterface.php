<?php
namespace tfeiszt\elasticable\search;

/**
 * Interface SearchInterface
 * @package tfeiszt\elasticable\search
 */
interface SearchInterface
{
    /**
     * @param string|mixed $query
     * @return mixed
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function setQuery($query);

    /**
     * @param array $fields
     * @return mixed
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function setFields(array $fields = []);

    /**
     * @param string $field
     * @return mixed
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function addField(string $field);
}
