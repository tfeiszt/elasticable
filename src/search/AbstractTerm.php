<?php
namespace tfeiszt\elasticable\search;

use Elastica\Query\BoolQuery;
use Elastica\Query\Term;
use tfeiszt\elasticable\query\SimpleFluentQuery;
use tfeiszt\elasticable\query\FluentInterface;

/**
 * Class AbstractTerm
 * @package tfeiszt\elasticable\search
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
abstract class AbstractTerm implements
    QueryableInterface,
    TermInterface
{
    /**
     * @var string|mixed
     */
    protected $query;
    /**
     * @var string
     */
    protected $term;
    /**
     * @var FluentInterface|null
     */
    protected $parent;

    /**
     * @return BoolQuery
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    abstract public function getSearchQuery() : BoolQuery;

    /**
     * AbstractSearch constructor.
     * @param FluentInterface|null $parent
     */
    public function __construct(FluentInterface $parent = null) {
        $this->parent = $parent;
    }

    /**
     * @param $name
     * @param $arguments
     * @return $this|false|mixed
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    function __call($name, $arguments){
        if ($this->parent instanceof SimpleFluentQuery) {
            $result = call_user_func_array(array($this->parent, $name), $arguments);
            if ($result instanceof SimpleFluentQuery) {
                return $this->parent;
            }
            return $result;
        }
    }

    /**
     * @param string $term
     * @return $this
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function setField(string $term)
    {
        $this->term = $term;
        return $this;
    }

    /**
     * @param string $query
     * @return $this
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function setValue($query)
    {
        $this->query = $query;
        return $this;
    }
}
