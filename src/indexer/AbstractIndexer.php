<?php
namespace tfeiszt\elasticable\indexer;

use Elastica\Mapping;
use tfeiszt\elasticable\ElasticableInterface;

/**
 * Class AbstractIndexer
 * @package tfeiszt\elasticable\indexer
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
abstract class AbstractIndexer
{
    /**
     * @var ElasticableInterface
     */
    protected $model;

    /**
     * AbstractIndexer constructor.
     * @param ElasticableInterface $model
     */
    public function __construct(ElasticableInterface $model)
    {
        $this->model = $model;
    }

    /**
     * @return ElasticableInterface
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param array $indexConfig
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function createIndex($indexConfig = [])
    {
        $this->getModel()
            ->getElasticaClient()
            ->getModelIndex($this->getModel())
            ->create($indexConfig, true);
        $this->getModel()->eventIndexCreate();
    }

    /**
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function createMappings()
    {
        $mapping = new Mapping();
        $mappings = $this->getModel()->getElasticMapping();
        $mapping->setProperties($mappings);
        $mapping->send($this->getModel()->getElasticaClient()->getModelIndex($this->getModel()));
        $this->getModel()->eventIndexMapping();
    }

    /**
     * @param false $mappingFirst
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function updateElasticDocument($mappingFirst = false)
    {
        if ($mappingFirst === true) {
            $this->createMappings();
        }

        $this->getModel()->eventIndexStart();
        $doc = $this->getModel()->getElasticDocument($this->getModel()->getElasticId());
        $this->getModel()->getElasticaClient()->getModelIndex($this->getModel())->addDocument($doc);
        $this->getModel()->eventIndexFinish();
    }
}
