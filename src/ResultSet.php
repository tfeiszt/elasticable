<?php
namespace tfeiszt\elasticable;

/**
 * Class ResultSet
 * @package tfeiszt\elasticable
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class ResultSet extends \Elastica\ResultSet
{
    /**
     * @var Pagination
     */
    protected $pagination = null;

    /**
     * @param string $modelClass
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function setModelClass(string $modelClass)
    {
        $this->modelClass = $modelClass;
    }

    /**
     * @param string $name
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getAggregationBucket($name)
    {
        $aggregation = $this->getAggregation($name);
        $result = [];
        foreach ($aggregation[$name]['buckets'] as $bucket) {
            $result[$bucket['key']] = $bucket['doc_count'];
        }
        return $result;
    }

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getAggregationBuckets()
    {
        $buckets = [];
        $aggregations = $this->getAggregations();
        foreach ($aggregations as $name => $aggregation) {
            $buckets[$name] = $this->getAggregationBucket($name);
        }
        return $buckets;
    }

    /**
     * @param Pagination $pagination
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function setPagination(Pagination $pagination)
    {
        $this->pagination = $pagination;
    }

    /**
     * @return array|false
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getPagination()
    {
        if ($this->pagination instanceof Pagination) {
            return [
                'page' => $this->pagination->getPage(),
                'size' => $this->pagination->getSize(),
                'total' => $this->getTotalHits()
            ];
        }
        return false;
    }
}
