<?php
namespace tfeiszt\elasticable;

use Elastica\Document;
use tfeiszt\elasticable\indexer\SingleIndexer;

/**
 * Trait ElasticableTrait
 * @package tfeiszt\elasticable
 */
trait ElasticableTrait
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getElasticIndexClass() : string
    {
        return Index::class;
    }

    /**
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getElasticIndexName(): string
    {
        $instance = new \ReflectionClass(static::class);
        return strtolower($instance->getShortName());
    }

    /**
     * @return Client
     * @throws \Exception
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getElasticaClient() : Client
    {
        return $this->client;
    }

    /**
     * @param Client $client
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function setElasticaClient(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function transformElasticData() : array
    {
        $result = [];
        foreach (static::getElasticMapping() as $fieldName => $mappingArgs) {
            if (is_object($this->getAttribute($fieldName))) {
                continue;
            }
            $result[$fieldName] = $this->getAttribute($fieldName);
        }
        return $result;
    }

    /**
     * @return Document
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getElasticDocument() : Document
    {
        return new Document($this->getElasticId(), $this->transformElasticData());
    }

    /**
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function updateElasticDocument() : void
    {
        $indexer = new SingleIndexer($this);
        $indexer->updateElasticDocument(false);
    }

    /**
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function eventIndexCreate() : void
    {
        return;
    }

    /**
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function eventIndexMapping() : void
    {
        return;
    }

    /**
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function eventIndexStart() : void
    {
        return;
    }

    /**
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function eventIndexFinish() : void
    {
        return;
    }
}
