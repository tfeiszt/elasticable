<?php
namespace tfeiszt\elasticable;

use Elastica\Document;

/**
 * Class AbstractElasticableModel
 * @package tfeiszt\elasticable
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
abstract class AbstractElasticableModel implements
    AttributesInterface,
    ElasticableInterface
{
    use ElasticableTrait;
    use AttributesTrait;

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    abstract public static function getElasticMapping(): array;

    /**
     * @return mixed
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    abstract public function getElasticId();
}
