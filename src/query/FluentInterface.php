<?php
namespace tfeiszt\elasticable\query;

use Elastica\Query;
use Elastica\Query\BoolQuery;

/**
 * Interface FluentInterface
 * @package tfeiszt\elasticable\query
 */
interface FluentInterface
{
    /**
     * @return Query
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function build() : Query;

    /**
     * @return BoolQuery
     * @author tamasfeiszt
     */
    public function buildConditions() : BoolQuery;
}
