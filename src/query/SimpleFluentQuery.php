<?php
namespace tfeiszt\elasticable\query;

use Elastica\Aggregation\Filter;
use Elastica\Aggregation\Terms;
use Elastica\Query;
use Elastica\Query\BoolQuery;
use tfeiszt\elasticable\search\FuzzySearch;
use tfeiszt\elasticable\search\MatchTerm;
use tfeiszt\elasticable\search\QueryableInterface;
use tfeiszt\elasticable\search\SimpleSearch;

/**
 * Class SimpleFluentQuery
 * @package tfeiszt\elasticable\query
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class SimpleFluentQuery implements FluentInterface
{
    /**
     * @var string
     */
    protected $operator = 'AND';
    /**
     * @var array
     */
    protected $queries = [];
    /**
     * @var array
     */
    protected $aggregations = [];
    /**
     * @var array
     */
    protected $sorts = [];

    /**
     * SimpleFluentQuery constructor.
     * @param BoolQuery|null $query
     */
    public function __construct(BoolQuery $query = null)
    {
        if ($query && $query instanceof BoolQuery) {
            $this->query = $query;
        } else {
            $this->query = new BoolQuery();
        }
    }

    /**
     * @param QueryableInterface $q
     * @return QueryableInterface
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    protected function addCondition(QueryableInterface $q)
    {
        $this->queries[] = [
            'query' => $q,
            'operator' => $this->operator
        ];
        return $q;
    }

    /**
     * @return $this
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function and()
    {
        $this->operator = 'AND';
        return $this;
    }

    /**
     * @return $this
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function not()
    {
        $this->operator = 'NOT';
        return $this;
    }

    /**
     * @return $this
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function or()
    {
        $this->operator = 'OR';
        return $this;
    }

    /**
     * @return MatchTerm
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function match()
    {
        $q = new MatchTerm($this);
        return $this->addCondition($q);
    }

    /**
     * @return SimpleSearch
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function search()
    {
        $q = new SimpleSearch($this);
        return $this->addCondition($q);
    }

    /**
     * @return FuzzySearch
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function fuzzy()
    {
        $q = new FuzzySearch($this);
        return $this->addCondition($q);
    }

    /**
     * @param FluentInterface $fluentQuery
     * @return $this
     * @author tamasfeiszt
     */
    public function query(FluentInterface $fluentQuery)
    {
        $this->queries[] = [
            'query' => $fluentQuery,
            'operator' => $this->operator
        ];
        return $this;
    }

    /**
     * @param string $name
     * @return $this
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function addAggregation($name)
    {
        $this->aggregations[] = $name;
        return $this;
    }

    /**
     * @param string $name
     * @param string $order
     * @param string $mode
     * @return $this
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function addSort($name, $order = 'ASC', $mode = '')
    {
        $sort = [
            $name => [
                'order' => $order
            ]
        ];
        if (!empty($mode)) {
            $sort[$name]['mode'] = $mode;
        }
        $this->sorts[] = $sort;
        return $this;
    }

    /**
     * @return BoolQuery
     * @author tamasfeiszt
     */
    public function buildConditions() : BoolQuery
    {
        foreach ($this->queries as $q) {
            if ($q['query'] instanceof FluentInterface) {
                $conditions = $q['query']->buildConditions();
                if ($q['operator'] === 'OR') {
                    $this->query->addShould($conditions);
                } elseif ($q['operator'] === 'NOT') {
                    $this->query->addMustNot($conditions);
                } else {
                    $this->query->addMust($conditions);
                }
            } else {
                if ($q['operator'] === 'OR') {
                    $this->query->addShould($q['query']->getSearchQuery());
                } elseif ($q['operator'] === 'NOT') {
                    $this->query->addMustNot($q['query']->getSearchQuery());
                } else {
                    $this->query->addMust($q['query']->getSearchQuery());
                }
            }
        }
        return $this->query;
    }

    /**
     * @return Query
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function build() : Query
    {
        $query = new Query();
        $query->setQuery($this->buildConditions());

        foreach ($this->aggregations as $aggregation) {
            $categoryAggregation = new Terms($aggregation);
            $categoryAggregation->setField($aggregation);
            $aggregation = new Filter($aggregation, $this->query);
            $aggregation->addAggregation($categoryAggregation);
            $query->addAggregation($aggregation);
        }
        foreach ($this->sorts as $sort){
            $query->addSort($sort);
        }
        return $query;
    }
}
