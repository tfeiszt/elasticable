<?php
namespace tfeiszt\elasticable;

/**
 * Class Pagination
 * @package tfeiszt\elasticable
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class Pagination
{
    /**
     * @var int
     */
    protected $page = 1;
    /**
     * @var int
     */
    protected $size;

    /**
     * Pagination constructor.
     * @param int $page
     * @param int $size
     */
    public function __construct(int $page, int $size)
    {
        if ($page < 1) {
            $page = 1;
        }
        $this->page = $page;
        if ($size < 1) {
            $size = 1;
        }
        $this->size = $size;
    }

    /**
     * @return int
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @return int
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @return float|int
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getOffset()
    {
        return ($this->page - 1) * $this->size;
    }
}
