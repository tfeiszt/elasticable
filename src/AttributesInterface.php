<?php
namespace tfeiszt\elasticable;

/**
 * Interface AttributesInterface
 * @package tfeiszt\elasticable
 */
interface AttributesInterface
{
    /**
     * @param array $attributes
     * @return mixed
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function setAttributes(array $attributes);

    /**
     * @param string $key
     * @return mixed
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getAttribute(string $key);

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getAttributes() : array;
}
