<?php
namespace tfeiszt\elasticable;

use Elastica\Index as ElasticaIndex;
use Elastica\ResultSet\BuilderInterface;
use Elastica\Search;

/**
 * Class Index
 * @package tfeiszt\elasticable
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class Index extends ElasticaIndex
{
    /**
     * @var ElasticableInterface
     */
    protected $model;

    /**
     * @var Pagination
     */
    protected $pagination = null;

    /**
     * @param ElasticableInterface $model
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function setModel(ElasticableInterface $model)
    {
        $this->model = $model;
    }

    /**
     * @return ElasticableInterface
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getName(): string
    {
        if ($this->getModel()) {
            return parent::getName() . '--' . strtolower($this->getModel()->getElasticIndexName());
        }

        return parent::getName();
    }

    /**
     * @param int $page
     * @param int $size
     * @return $this
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function setPagination($page, $size)
    {
        $this->pagination = new Pagination($page, $size);
        return $this;
    }

    /**
     * @param string $query
     * @param array|null $options
     * @param BuilderInterface|null $builder
     * @return Search
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function createSearch($query = '', $options = null, ?BuilderInterface $builder = null): Search
    {
        if ($builder === null) {
            $builder = new Builder($this->pagination);
            if ($this->getModel()) {
                $builder->setModel($this->getModel());
            }
        }
        if ($this->pagination instanceof Pagination) {
            $query->setSize($this->pagination->getSize());
            $query->setFrom($this->pagination->getOffset());
        }
        $search = parent::createSearch($query, $options, $builder);
        return $search;
    }
}
