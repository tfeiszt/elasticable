<?php
namespace tfeiszt\elasticable;

use Elastica\Document;

/**
 * Interface ElasticableInterface
 * @package tfeiszt\elasticable
 */
interface ElasticableInterface
{
    /**
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getElasticIndexName() : string;

    /**
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getElasticIndexClass() : string;

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getElasticMapping() : array;

    /**
     * @return mixed
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getElasticId();

    /**
     * @return Client
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getElasticaClient() : Client;

    /**
     * @param Client $client
     * @return mixed
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function setElasticaClient(Client $client);

    /**
     * @param string $key
     * @return mixed
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getAttribute(string $key);

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function transformElasticData() : array;

    /**
     * @return Document
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getElasticDocument() :Document;

    /**
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function updateElasticDocument() : void;

    //== Events

    /**
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function eventIndexCreate() : void;

    /**
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function eventIndexMapping() : void;

    /**
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function eventIndexStart() : void;

    /**
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function eventIndexFinish() : void;
}
